from django.urls import path, include
from . import views


urlpatterns = [
    path("redirect_to_receiptlist/", views.redirect_to_receiptlist),
    path("", views.redirect_to_receiptlist, name="home"),
]
