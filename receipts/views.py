from django.shortcuts import render, redirect
from .models import Receipt

# Create your views here.


def list_receipt(request):
    receipt_list = Receipt.objects.all()
    context = {
         "receipt_list": receipt_list
     }
    return render(request, "receipt/detail.html", context)


def redirect_to_receiptlist(request):
    return redirect("home")
